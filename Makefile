default:
	asciidoctor -v workshop/content/index.adoc -D public -a gitlab_hosted=true
	find ./public -type f -exec sed -i "s/%USERNAME%/lab-user/g" {} \;
	find ./public -type f -exec sed -i "s/%PASSWORD%/\&lt;PASSWORD\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%IP_ADDRESS%/\&lt;IP_ADDRESS\&gt;/g" {} \;

